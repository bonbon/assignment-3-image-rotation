#include "bmp.h"
#include "command_arguments.h"
#include "image.h"
#include "messages.h"
#include "rotate.h"
#include "utils.h"
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
  struct command_arguments args = { 0 };
  const int args_status = command_arguments_get(argc, argv, &args);
  if (args_status != 0) {
    print_error(USAGE_MESSAGE);
    return 1;
  }

  FILE* source_file = fopen(args.source, "r");
  if (source_file == NULL) {
    perror("Couldn't open the source file");
    return 1;
  }

  struct image image = { 0 };
  const enum bmp_read_status read_status = from_bmp(source_file, &image);
  fclose(source_file);
  if (read_status != BMP_READ_OK) {
    // NOTE: If read_status is not BMP_READ_OK `from_bmp` doesn't allocate any memory
    print_error("Couldn't read BMP image from source file: ");
    print_error(BMP_READ_STATUS_MESSAGES[read_status]);
    print_error("\n");
    return 1;
  }
  struct image rotated = rotate_by_angle(image, args.angle);
  image_free(image);
  if (rotated.data == NULL) {
    perror("Couldn't allocate necessary memory");
    return 1;
  }

  FILE* output_file = fopen(args.target, "w");
  if (output_file == NULL) {
    perror("Couldn't open the output file");
  }
  const enum bmp_write_status write_status = to_bmp(output_file, &rotated);
  fclose(output_file);
  image_free(rotated);
  if (write_status != BMP_WRITE_OK) {
    print_error("Couldn't write BMP image to output file: ");
    print_error(BMP_WRITE_STATUS_MESSAGES[write_status]);
    print_error("\n");
    return 1;
  }
	return 0;
}
