#include "command_arguments.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

static bool is_angle_allowed(int16_t angle) {
  return -270 <= angle && angle <= 270 && angle % 90 == 0;
}

static bool is_numerical(char* string) {
  char* current = string;
  if (*current == '-' || *current == '+') {
    current ++;
  }

  if (*current == '\0') {
    return false;
  }

  while (*current != '\0') {
    if (*current < '0' || '9' < *current) {
      return false;
    }
    current ++;
  }
  return true;
}

int command_arguments_get(int argc, char** argv, struct command_arguments* arguments) {
  if (argc < 4) {
    return 1;
  }
  arguments->source = argv[1];
  arguments->target = argv[2];

  if (!is_numerical(argv[3])) {
    return 1;
  }

  char* end;
  int16_t angle = (int16_t) strtol(argv[3], &end, 10);
  if (!is_angle_allowed(angle)) {
    return 1;
  }
  arguments->angle = angle;
  return 0;
}
