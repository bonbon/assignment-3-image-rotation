#include "image.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

typedef uint64_t pixel_index_getter(uint64_t width, uint64_t height, uint64_t row, uint64_t column);

static uint64_t pixel_index_getter_0(uint64_t width, uint64_t height __attribute__ ((unused)), uint64_t row, uint64_t column) {
  return width * row + column;
}

static uint64_t pixel_index_getter_90(uint64_t width, uint64_t height __attribute__ ((unused)), uint64_t row, uint64_t column) {
  return width * column + width - 1 - row;
}

static uint64_t pixel_index_getter_180(uint64_t width, uint64_t height, uint64_t row, uint64_t column) {
  return width * (height - row - 1) + (width - column - 1);
}

static uint64_t pixel_index_getter_270(uint64_t width, uint64_t height, uint64_t row, uint64_t column) {
  return width * (height - column - 1) + row;
}

static pixel_index_getter* const ROTATIONS_TO_PIXEL_INDEX_GETTER[4] = {
  pixel_index_getter_0,
  pixel_index_getter_90,
  pixel_index_getter_180,
  pixel_index_getter_270
};

struct image rotate(struct image const source, uint8_t rotations) {
  if (source.data == NULL) {
    return (struct image) { 0 };
  }

  struct pixel* pixels = malloc(sizeof(struct pixel) * source.width * source.height);
  const uint64_t new_height = rotations % 2 == 0 ? source.height : source.width;
  const uint64_t new_width = rotations % 2 == 0 ? source.width : source.height;

  if (pixels == NULL) {
    return (struct image) { 0 };
  }

  pixel_index_getter* pixel_index_getter_rotated = ROTATIONS_TO_PIXEL_INDEX_GETTER[rotations % 4];
  for (size_t i = 0; i < new_height; i ++) {
    for (size_t j = 0; j < new_width; j ++) {
      uint64_t current = pixel_index_getter_0(new_width, new_height, i, j);
      uint64_t rotated_index = pixel_index_getter_rotated(source.width, source.height, i, j);
      pixels[current] = source.data[rotated_index];
    }
  }

  return (struct image) {
    .width = new_width,
    .height = new_height,
    .data = pixels
  };
}

struct image rotate_by_angle(struct image const source, int16_t angle) {
  const uint16_t positive_angle = angle < 0 ? angle + 360 : angle;
  const uint16_t rotations = positive_angle / 90;
  return rotate(source, rotations);
}
