#include <stdio.h>

int print_error(const char* string) {
  return fprintf(stderr, "%s", string);
}
