#include "image.h"
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

void image_free(struct image image) {
  free(image.data);
  image.data = NULL;
}

struct image image_copy(struct image const image) {
  const size_t data_size = sizeof(struct pixel) * image.width * image.height;
  if (data_size == 0 || image.data == NULL) {
    return (struct image) { 0 };
  }
  struct pixel* copied_data = malloc(data_size);
  if (copied_data == NULL) {
    return (struct image) { 0 };
  }
  // memcpy is unsafe, but it's the only way apart from copying bits manually
  // NOLINTNEXTLINE
  memcpy(copied_data, image.data, data_size);
  return (struct image) {
    .width = image.width,
    .height = image.height,
    .data = copied_data
  };
}
