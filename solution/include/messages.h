#pragma once

#include "bmp.h"

static const char* const USAGE_MESSAGE = "usage:\n\
  image-transformer <source-image> <transformed-image> <angle>\n\
  <source-image> - ...\n\
  <transformed-image> - ...\n\
  <angle> - 0, 90, -90, 180, -180, 270, -270\n";

static const char* const BMP_WRITE_STATUS_MESSAGES[BMP_WRITE_COUNT] = {
  [BMP_WRITE_OK] = "Ok",
  [BMP_WRITE_ERROR] = "Couldn't write to the file"
};

static const char* const BMP_READ_STATUS_MESSAGES[BMP_READ_COUNT] = {
  [BMP_READ_OK] = "Ok",
  [BMP_READ_INVALID_FORMAT] = "Invalid format",
  [BMP_READ_FILE_ERROR] = "Couldn't read from file",
  [BMP_READ_MALLOC_ERROR] = "Couldn't allocate neccessary memory"
};
