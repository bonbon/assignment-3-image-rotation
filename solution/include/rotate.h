#pragma once

#include "image.h"

struct image rotate(struct image const source);

struct image rotate_by_angle(struct image const source, int16_t angle);
