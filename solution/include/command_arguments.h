#pragma once

#include <inttypes.h>
#include <stdbool.h>

struct command_arguments {
  const char* source;
  const char* target;
  int16_t angle;
};

int command_arguments_get(int argc, char** argv, struct command_arguments* arguments);
