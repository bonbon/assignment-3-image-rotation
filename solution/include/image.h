#pragma once

#include <inttypes.h>

struct  __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void image_free(struct image image);

struct image image_copy(struct image const image);
