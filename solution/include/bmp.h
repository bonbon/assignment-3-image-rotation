#pragma once

#include "image.h"
#include <stdio.h>

struct  __attribute__((packed)) bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

enum bmp_read_status {
  BMP_READ_OK = 0,
  BMP_READ_INVALID_FORMAT,
  BMP_READ_FILE_ERROR,
  BMP_READ_MALLOC_ERROR,
  BMP_READ_COUNT
};

enum bmp_read_status from_bmp(FILE* file, struct image* dist);

enum bmp_write_status {
  BMP_WRITE_OK = 0,
  BMP_WRITE_ERROR,
  BMP_WRITE_COUNT
};

enum bmp_write_status to_bmp(FILE* out, struct image const* image);
